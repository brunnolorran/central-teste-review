import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { CgBrands } from "@/data";
import { motion, useViewportScroll, useTransform } from "framer-motion";

// import ServiceCardThree from "@/components/service-card-three";

const ServiceThree = () => {
  const { scrollYProgress } = useViewportScroll();
  const scale = useTransform(scrollYProgress, [0, 1], [0, 1.5]);

  const { academy, editora, music, negocios } = CgBrands;

  return (
    <section className="commonSection banner-brand-white">
      <Container>
        <Row>
          <Col lg={12} className="text-center">
            <motion.h2 style={{ scale }} className="sec_title pb-5">
              Noss<span>as Ma</span>rcas
            </motion.h2>
          </Col>
        </Row>
        <Row>
          <Col lg={3} md={12}>
            <div className="text-right">
              <h5 className="colorBlackBrands titleCardsCG">
                Central Gospel <br /> Academy
              </h5>
              <p className="textContentCG">
                Cursos EAD e presenciais para o desenvolvimento e o exercício da
                fé em diversas áreas.
              </p>
              <motion.a
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
                className="btnYellow btnStyle1"
                href="https://www.instagram.com/centralgospelacademy/"
                target="_blank"
              >
                SAIBA MAIS
              </motion.a>
            </div>
          </Col>
          <Col lg={3} md={12} className="mobile_mb_1 mobile_mt_1 text-right">
            <img src={academy} alt="academy" width="270" />
          </Col>
          <Col lg={3} md={12} className="mobile_mb_1 text-left">
            <img src={editora} alt="editora" width="270" />
          </Col>
          <Col lg={3} md={12}>
            <div>
              <h5 className="colorBlackBrands titleCardsCG">
                Central Gospel <br />{" "}
                <span className="colorYellowBrands">Editora</span>
              </h5>
              <p className="textContentCG">
                Livros literários, obras teológicas e Bíblias para incrementar
                os estudos cristãos.
              </p>
              <motion.a
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
                className="btnYellow btnStyle1"
                href="https://www.instagram.com/editora_centralgospel/"
                target="_blank"
              >
                SAIBA MAIS
              </motion.a>
            </div>
          </Col>
        </Row>
        <Row className="mt-5">
          <Col lg={3} md={12}>
            <div className="text-right">
              <h5 className="colorBlackBrands titleCardsCG">
                Central Gospel <br />
                <span className="colorOrangeBrands">Negócios</span>
              </h5>
              <p className="textContentCG">
                Empreendedorismo com propósito para alcançar e transbordar as
                bênçãos de uma vida plena.
              </p>
              <motion.a
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
                className="btnYellow btnStyle1"
                href="https://www.instagram.com/centralgospelnegocios/"
                target="_blank"
              >
                SAIBA MAIS
              </motion.a>
            </div>
          </Col>
          <Col lg={3} md={12} className="mobile_mb_1 mobile_mt_1 text-right">
            <img src={negocios} alt="negocios" width="270" />
          </Col>
          <Col lg={3} md={12} className="mobile_mb_1 text-left">
            <img src={music} alt="music" width="270" />
          </Col>
          <Col lg={3} md={12}>
            <div>
              <h5 className="colorBlackBrands titleCardsCG">
                Central Gospel <br />{" "}
                <span className="colorRedBrands">Music</span>
              </h5>
              <p className="textContentCG">
                O melhor do louvor cristão — artistas, vídeo clipes e álbuns
                evangélicos em um só lugar.
              </p>
              <motion.a
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
                class="btnYellow btnStyle1 hover-filled-slide-left"
                href="https://www.instagram.com/cgospelmusic/"
                target="_blank"
              >
                <span>SAIBA MAIS</span>
              </motion.a>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default ServiceThree;
