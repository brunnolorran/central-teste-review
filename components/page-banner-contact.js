import React from "react";

const PageBannerContact = ({ title, name }) => {
  return (
    <section className="pageBannerContact">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="banner_content text-center">
              <h2>{title}</h2>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default PageBannerContact;
