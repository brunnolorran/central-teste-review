import React, { useEffect } from "react";
import { useInView } from "react-intersection-observer";
import { motion, useAnimation } from "framer-motion";

const PageBannerContact = ({ title, name }) => {
  const controls = useAnimation();
  const [ref, inView] = useInView();

  useEffect(() => {
    if (inView) {
      controls.start("visible");
    }
  }, [inView]);

  return (
    <section className="bgHome position-home-relative">
      <motion.div
        ref={ref}
        animate={controls}
        animate={{
          x: -100,
          y: 0,
          scale: 1,
          rotate: 0,
        }}
        transition={{ duration: 1 }}
        className="col-lg-5  text-about-content position-home"
      >
        <p>
          Hoje somos um grupo baseado nos{" "}
          <span>princípios e valores cristãos.</span> Acreditamos que podemos
          mudar o mundo com foco nas pessoas e no seu crescimento.
        </p>
      </motion.div>
    </section>
  );
};

export default PageBannerContact;
