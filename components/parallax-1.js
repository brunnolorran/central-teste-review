import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { TeamAbout } from "@/data";

const ParallaxOne = () => {
  const { presidente, vice, executiva } = TeamAbout;

  return (
    <section className="commonSection bg-about-yellow mt-5">
      <Container>
        <Row>
          <Col lg={{ span: 10, offset: 1 }} sm={12} className="text-center ">
            <div className="testimonial_content">
              <Col lg={12} sm={12} className="text-center mb-5">
                <img src={presidente} alt="presidente" className="img-fluid" />
                <h3 className="mt-3 mb-2">
                  PR. SILAS & ELIZETE{" "}
                  <span className="font-weight-bold">MALAFAIA</span>
                </h3>
                <h5>
                  <span className="bg_black_team">PRESIDENTE, FUNDADORES</span>
                </h5>
              </Col>
              <Row>
                <Col lg={6} sm={12} className="text-center mb-5">
                  <img src={vice} alt="vice-presidente" className="img-fluid" />
                  <h3 className="mt-3 mb-2">
                    SILAS MALAFAIA{" "}
                    <span className="font-weight-bold">FILHO</span>
                  </h3>
                  <h5>
                    <span className="bg_black_team">VICE - PRESIDENTE</span>
                  </h5>
                  <a href="#">
                    <i className="fa fa-facebook mr-2"></i>
                  </a>
                  <a
                    href="https://www.instagram.com/prsilasfilho/"
                    target="_blank"
                  >
                    <i className="fa fa-instagram ml-2"></i>
                  </a>
                </Col>

                <Col lg={6} sm={12} className="text-center mb-5">
                  <img
                    src={executiva}
                    alt="diretora executiva"
                    className="img-fluid"
                  />
                  <h3 className="mt-3 mb-2">
                    ELBA <span className="font-weight-bold">ALENCAR</span>
                  </h3>
                  <h5>
                    <span className="bg_black_team">DIRETORA EXECUTIVA</span>
                  </h5>
                  <a href="#">
                    <i className="fa fa-facebook mr-2"></i>
                  </a>
                  <a
                    href="https://www.instagram.com/elbaalencar/"
                    target="_blank"
                  >
                    <i className="fa fa-instagram ml-2"></i>
                  </a>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default ParallaxOne;
