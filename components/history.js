import React from "react";
import { ImageHistory } from "@/data";
import { Col, Container, Row } from "react-bootstrap";
import { motion, useViewportScroll, useTransform } from "framer-motion";

const History = () => {
  const { scrollYProgress } = useViewportScroll();
  const scale = useTransform(scrollYProgress, [0, 1], [0, 1.5]);

  const { history } = ImageHistory;
  return (
    <section className="commonSection ab_agency">
      <Container>
        <Row>
          <Col lg={12} md={12} sm={12} className="PR_79 text-center">
            <motion.h2 style={{ scale }} className="sec_title mb-3 text-center">
              Nos<span>sa Hist</span>ória
            </motion.h2>
            <h5 className="text-center subtitle-about mb-5 font-weight-bold">
              Um pouco da nossa trajetória <br /> de 21 anos no mercado.
            </h5>
            <img
              src={history}
              alt="nossa história"
              className="img-fluid m20"
              width="650"
            />
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default History;
