import React, { useEffect } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { useInView } from "react-intersection-observer";
import { motion, useAnimation } from "framer-motion";

const PageBannerAbout = ({ title, name }) => {
  const controls = useAnimation();
  const [ref, inView] = useInView();

  useEffect(() => {
    if (inView) {
      controls.start("visible");
    }
  }, [inView]);
  return (
    <section className="pageBannerAbout">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="container-about-position">
              <div className="title-about-position">
                <motion.h2
                  style={{ marginBottom: 100 }}
                  ref={ref}
                  animate={controls}
                  animate={{
                    x: 0,
                    y: 90,
                    scale: 1,
                    rotate: 0,
                  }}
                  transition={{ duration: 1 }}
                  className="sec_title pb-3 pt-5"
                >
                  So<span>bre N</span>ós
                </motion.h2>
                <Col lg={10} md={12} className="text-about-content">
                  <motion.p
                    ref={ref}
                    animate={controls}
                    initial={{ scale: 0 }}
                    animate={{ scale: 1 }}
                    transition={{
                      type: "spring",
                      stiffness: 260,
                      damping: 20,
                    }}
                    transition={{ duration: 1.5 }}
                  >
                    Nascemos como uma editora em 1999 para apoiar a Associação
                    Vitória em Cristo (AVEC) com materiais evangelísticos.
                    Através do crescimento exponencial surgiram novas frontes,
                    como a Central Gospel Music. Posteriormente, avançamos para
                    o Empreendedorismo e Ensino. Nos tornamos, portanto, uma
                    rede central que leva a Palavra de Deus através de livros,
                    canções, negócios, artes e estudos.
                  </motion.p>
                  <motion.p
                    ref={ref}
                    animate={controls}
                    initial={{ scale: 0 }}
                    animate={{ scale: 1 }}
                    transition={{
                      type: "spring",
                      stiffness: 260,
                      damping: 20,
                    }}
                    transition={{ duration: 2 }}
                  >
                    Acreditamos que todos nós fomos criados por Deus para
                    alcançarmos vida plena, e a diferença encontra-se no nível
                    de conhecimento de cada um. Por esse motivo, alguns não
                    conseguem desenvolver-se plenamente em suas vidas. O
                    conhecimento é o que liberta o ser humano das algemas da
                    ignorância, fazendo-o compreender todo o seu potencial
                    divino. O nosso{" "}
                    <span className="font-weight-bold">propósito</span> é trazer
                    à tona esse conhecimento pleno de quem Deus é e de quem nós
                    somos em Deus em todos os sentidos existenciais.
                  </motion.p>
                </Col>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default PageBannerAbout;
