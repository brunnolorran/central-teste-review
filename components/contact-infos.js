import React, { useEffect } from "react";
import { useInView } from "react-intersection-observer";
import {
  motion,
  useAnimation,
  useViewportScroll,
  useTransform,
} from "framer-motion";
import { ValuesHome } from "@/data";

const ContactInfos = () => {
  const controls = useAnimation();
  const [ref, inView] = useInView();
  const { scrollYProgress } = useViewportScroll();
  const scale = useTransform(scrollYProgress, [0, 1], [0, 1.5]);

  useEffect(() => {
    if (inView) {
      controls.start("visible");
    }
  }, [controls, inView]);

  return (
    <section className="commonSection client_2 bgPeoples commonSectionValues">
      <div className="container space-custom-home">
        <div className="row">
          <div className="col-lg-12 text-center">
            <motion.h2
              style={{ scale }}
              className="sec_title mb-3 text-center mb-5"
            >
              V<span>ALORE</span>S
            </motion.h2>
          </div>
        </div>
        <motion.div
          ref={ref}
          animate={controls}
          initial="hidden"
          variants={{
            visible: { opacity: 1, scale: 1 },
            hidden: { opacity: 0, scale: 0 },
          }}
          transition={{ duration: 1 }}
          className="row"
        >
          {ValuesHome.map(({ title, infos }, index) => (
            <div
              className="col-lg-4 col-sm-6 col-md-3 text-center mt-5"
              key={`contact-infos-${index}`}
            >
              <div className="singleClient_2">
                <h3 className="font-weight-bold">{title}</h3>
                {infos.map(({ name }, index) => (
                  <p key={`contact-infos-list-${index}`}>{name}</p>
                ))}
              </div>
            </div>
          ))}
        </motion.div>
      </div>
    </section>
  );
};

export default ContactInfos;
