import React from "react";
import Footer from "@/components/footer";
import Layout from "@/components/layout";
import PageBannerBrands from "@/components/page-banner-our-brands";
import ServiceThree from "@/components/service-three";
import MenuContextProvider from "@/context/menu-context";
import SearchContextProvider from "@/context/search-context";
import HeaderOne from "@/components/header-one";

const ServicePage = () => {
  return (
    <MenuContextProvider>
      <SearchContextProvider>
        <Layout PageTitle="Nossas Marcas">
          <HeaderOne />
          <PageBannerBrands />
          <ServiceThree />
          <Footer />
        </Layout>
      </SearchContextProvider>
    </MenuContextProvider>
  );
};

export default ServicePage;
