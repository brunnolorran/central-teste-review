import logoLight from "@/images/logo.png";
import logoDark from "@/images/logo2.png";
import logoDarkSmall from "@/images/logo3.png";
import logoYellowSmall from "@/images/logo4.png";

export const LogoImage = {
  light: logoLight,
  dark: logoDark,
  darkSmall: logoDarkSmall,
  yellowSmall: logoYellowSmall,
};

import imageAcademy from "@/images/our-brands/cg-academy.jpg";
import imageEditora from "@/images/our-brands/cg-editora.jpg";
import imageMusic from "@/images/our-brands/cg-music.jpg";
import imageNegocios from "@/images/our-brands/cg-negocios.jpg";

export const CgBrands = {
  academy: imageAcademy,
  editora: imageEditora,
  music: imageMusic,
  negocios: imageNegocios,
};

import imagePresident from "@/images/team/presidente.png";
import imageVice from "@/images/team/vice.jpg";
import imageExecutive from "@/images/team/executiva.jpg";

export const TeamAbout = {
  presidente: imagePresident,
  vice: imageVice,
  executiva: imageExecutive,
};

import history from "@/images/history.png";

export const ImageHistory = {
  history: history,
};

export const NavLinks = [
  {
    name: "Home",
    url: "/",
  },
  {
    name: "Sobre Nós",
    url: "/about",
  },
  {
    name: "Nossas Marcas",
    url: "/our-brands",
  },
  {
    name: "Contato",
    url: "/contact",
  },
];

export const ValuesHome = [
  {
    title: "MISSÃO",
    infos: [
      {
        name:
          "Juntos, vamos apresentar o conhecimento da verdade, apresentando-o em todos os formatos e de todas as formas, com o objetivo de mudar a vida das pessoas e, por conseguinte, da sociedade.",
      },
    ],
  },
  {
    title: "VISÃO",
    infos: [
      {
        name:
          "Empreender com resiliência. Sonhar com sabedoria. Expressar arte com valor. Ensinar com determinação. Família, oficio, saúde e propósito. A partir da plenitude e da excelência, nós desenvolvemos pessoas e formamos líderes.",
      },
    ],
  },
  {
    title: "VALORES",
    infos: [
      {
        name:
          "Fé para alcançar o sobrenatural. Excelência para com as responsabilidades. Dedicação para ir além. Coragem para as oportunidades. Criatividade para fazer o novo e transformação para viver o novo.",
      },
    ],
  },
];

export const ContactFormTitle = {
  title: "Fale Conosco",
};

// export const ServicePostThreeData = {
//   sectionContent: {
//     title: "Nossas Marcas",
//     subTitle: "what we do",
//     text:
//       "We are committed to providing our customers with exceptional service \n while offering our employees the best training.",
//   },
//   posts: [
//     {
//       title: "web development",
//       image: serviceOne01,
//       url: "/service-details",
//     },
//     {
//       title: "digital marketing",
//       image: serviceOne02,
//       url: "/service-details",
//     },
//     {
//       title: "product branding",
//       image: serviceOne03,
//       url: "/service-details",
//     },
//   ],
// };
